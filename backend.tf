//backend.tf
terraform {
  backend "s3" {
    encrypt = true
    key     = "terraform-pipeline/terraform.tfstate" //rename up tho the '/' what ever you like
    region  = "eu-west-2"
  }
}